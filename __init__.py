from st3m.application import Application, ApplicationContext
from st3m.input import InputState, InputController
from ctx import Context
import st3m.run
import os
import leds
import math

class Pictures(Application):
    input = InputController()
    since_state_change: int = 0

    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        
        # For colors
        self._scale = 1.0
        self._led = 0.0
        self._phase = 0.0
        
        # For images
        # Find correct path depending on how the app was installed
        applist = os.listdir("/flash/sys/apps/")
        if "pictures" in applist:
            self.cwd = "/flash/sys/apps/pictures"
        elif "Monster-pictures" in applist:
            self.cwd = "/flash/sys/apps/Monster-pictures"
            
        self.imageList = os.listdir(str(self.cwd) + "/images/")
        print(self.imageList)
        self.numberOfImages = len(os.listdir(str(self.cwd) + "/images/"))
        self.imageCounter = 0
        self.imagePath = self.cwd + f"/images/{self.imageList[self.imageCounter]}"

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)

        # Change colors slightly every iteration
        self._phase += delta_ms / 1000
        self._scale = math.sin(self._phase)
        self._led += delta_ms / 45
        if self._led >= 40:
            self._led = 0

        # Listen for button presses in order to change the image
        if self.input.buttons.app.left.pressed:
            self.imageCounter -= 1
            if self.imageCounter < 0:
                self.imageCounter = self.numberOfImages - 1
            self.imagePath = self.cwd + f"/images/{self.imageList[self.imageCounter]}"
        elif self.input.buttons.app.right.pressed:
            self.imageCounter += 1
            if self.imageCounter >= self.numberOfImages:
                self.imageCounter = 0
            self.imagePath = self.cwd + f"/images/{self.imageList[self.imageCounter]}"

    def draw(self, ctx: Context) -> None:
        # Set LED color
        leds.set_hsv(int(self._led), abs(self._scale) * 360, 1, 0.2)
        leds.update()

        # Draw image
        ctx.image_smoothing = False
        ctx.rectangle(-120, -120, 240, 240)
        ctx.rgb(0, 0.60, 0.1)
        ctx.fill()

        ctx.image(
            self.imagePath,
            -120,
            -120,
            240,
            240,
        )

# Continue to make runnable via mpremote run.
if __name__ == '__main__':
    st3m.run.run_view(Pictures(ApplicationContext()))

